CREATE TABLE Complejos(
  ID           NUMBER(10)    NOT NULL,
  nombre       VARCHAR(50)  NOT NULL,
  direccion    VARCHAR(200)  NOT NULL,
  lat      VARCHAR(10)   NOT NULL,
  lon     VARCHAR(10)   NOT NULL,
  CONSTRAINT complejos_pk PRIMARY KEY (ID)
);

CREATE SEQUENCE complejos_seq;

CREATE OR REPLACE TRIGGER complejos_bir 
BEFORE INSERT ON Complejos 
FOR EACH ROW

BEGIN
  SELECT complejos_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO Complejos VALUES(null, 'Oakland','zona 10 , Guatemala, Guatemala','50.54545', '14.5456465' );
INSERT INTO Complejos VALUES(null, 'Miraflores','zona 7, Guatemala, Guatemala','50.54545', '14.5456465' );
INSERT INTO Complejos VALUES(null, 'Cayala','zona 16, Guatemala, Guatemala','50.54545', '14.5456465' );
INSERT INTO Complejos VALUES(null, 'Portales','zona 18, Guatemala, Guatemala','50.54545', '14.5456465' );

SELECT * FROM Complejos;


CREATE TABLE Sala(
  ID           NUMBER(10)    NOT NULL,
  nombre       VARCHAR(50)   NOT NULL,
  tipo         VARCHAR(200)  NOT NULL,
  idComplejo   NUMBER(10)    NOT NULL,
  CONSTRAINT sala_pk PRIMARY KEY (ID),  
  CONSTRAINT Sala_fk FOREIGN KEY (idComplejo) REFERENCES Complejos(ID) ON DELETE CASCADE
);

CREATE SEQUENCE sala_seq;

CREATE OR REPLACE TRIGGER sala_bir 
BEFORE INSERT ON Sala 
FOR EACH ROW

BEGIN
  SELECT Sala_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/* Complejo 1*/
INSERT INTO Sala VALUES(null, '1','3D', 1 );
INSERT INTO Sala VALUES(null, '2','3D', 1 );
INSERT INTO Sala VALUES(null, '3','3D', 1 );
INSERT INTO Sala VALUES(null, '1n','Normal', 1 );
INSERT INTO Sala VALUES(null, '2n','Normal', 1 );
INSERT INTO Sala VALUES(null, '3n','Normal', 1 );
INSERT INTO Sala VALUES(null, '4n','Normal', 1 );
INSERT INTO Sala VALUES(null, '5n','Normal', 1 );
INSERT INTO Sala VALUES(null, '6n','Normal', 1 );


/* complejo 2*/
INSERT INTO Sala VALUES(null, '1','3D', 2 );
INSERT INTO Sala VALUES(null, '2','3D', 2 );
INSERT INTO Sala VALUES(null, '3n','Normal', 2 );
INSERT INTO Sala VALUES(null, '4n','Normal', 2 );
INSERT INTO Sala VALUES(null, '5n','Normal', 2 );
INSERT INTO Sala VALUES(null, '6n','Normal', 2 );

/*Complejo 3 */
INSERT INTO Sala VALUES(null, '1n','Normal', 3 );
INSERT INTO Sala VALUES(null, '2n','Normal', 3 );
INSERT INTO Sala VALUES(null, '3n','Normal', 3 );
INSERT INTO Sala VALUES(null, '4n','Normal', 3 );
INSERT INTO Sala VALUES(null, '5n','Normal', 3 );
INSERT INTO Sala VALUES(null, '6n','Normal', 3 );
INSERT INTO Sala VALUES(null, '7n','Normal', 3 );
INSERT INTO Sala VALUES(null, '8n','Normal', 3 );

/*complejo 4*/
INSERT INTO Sala VALUES(null, '1','3D', 4 );
INSERT INTO Sala VALUES(null, '2','3D', 4 );
INSERT INTO Sala VALUES(null, '3','3D', 4 );
INSERT INTO Sala VALUES(null, '4','3D', 4 );
INSERT INTO Sala VALUES(null, '5','3D', 4 );
INSERT INTO Sala VALUES(null, '6','3D', 4 );
INSERT INTO Sala VALUES(null, '1n','Normal', 4 );
INSERT INTO Sala VALUES(null, '2n','Normal', 4 );
INSERT INTO Sala VALUES(null, '3n','Normal', 4 );
INSERT INTO Sala VALUES(null, '4n','Normal', 4 );
SELECT * FROM Sala;


CREATE TABLE Clasificacion(
  ID           NUMBER(10)    NOT NULL,
  nombre       VARCHAR(50)   NOT NULL,
  descripcion  VARCHAR2(500)  NOT NULL,
  CONSTRAINT clasificacion_pk PRIMARY KEY (ID)
);

CREATE SEQUENCE clasificacion_seq;

CREATE OR REPLACE TRIGGER cladificacion_bir 
BEFORE INSERT ON Clasificacion
FOR EACH ROW

BEGIN
  SELECT Clasificacion_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO Clasificacion VALUES(null, 'TP','Todo Publico');
INSERT INTO Clasificacion VALUES(null, 'A','Menores de 12 acompa�ados por adulto');
INSERT INTO Clasificacion VALUES(null, 'B','Mayores de 12 A�os');
INSERT INTO Clasificacion VALUES(null, 'C','Mayores de 15 A�os');
INSERT INTO Clasificacion VALUES(null, 'D','Mayores de 18 A�os');

SELECT * FROM CLASIFICACION;


CREATE TABLE Pelicula(
  ID              NUMBER(10)    NOT NULL,
  nombre          VARCHAR2(255)   NOT NULL,
  descripcion     VARCHAR2(1200)  NOT NULL,
  imagen          VARCHAR2(255)   NOT NULL,
  trailer         VARCHAR2(500)  NOT NULL,
  duracion        VARCHAR2(255)  NOT NULL,
  idClasificacion NUMBER(10)    NOT NULL,
  CONSTRAINT Pelicula_pk PRIMARY KEY (ID),
  CONSTRAINT Pelicula_fk FOREIGN KEY (idClasificacion) REFERENCES Clasificacion(ID) ON DELETE CASCADE
);

CREATE SEQUENCE pelicula_seq;

CREATE OR REPLACE TRIGGER pelicula_bir 
BEFORE INSERT ON Pelicula
FOR EACH ROW

BEGIN
  SELECT pelicula_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;


Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (1,'Rompiendo Las Reglas','Jake Tyler recientemente se ha mudado a Orlando, Florida, con su familia para apoyar a su hermano menor en una carrera como tenista profesional. Jake era una estrella atleta en el equipo de f�tbol de su casa, pero en esta nueva ciudad es un extranjero con una reputaci�n por ser un luchador de mal genio despu�s de ver un v�deo de �l en Internet comenzando una pelea en un juego de f�tbol, porque uno de sus oponentes hizo un comentario sobre su padre muerto, y �ste v�deo circula en su nueva escuela. En dicho video sale Bole, conocido ladr�n de los alrededores.
Tratando de hacer un intento por encajar, en la invitaci�n de una compa�era de clase, Baja Miller, Jake va a una fiesta donde es obligado a una pelea con el campe�n de artes marciales ','imagenes/Pelicula1.jpg','https://www.youtube.com/embed/WTVTcDe5TXM','1:35',3);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (2,'Super Cool',' Los chicos Seth, Evan y Fogell (m�s adelante llamado "McLovin") tienen como destino una fiesta de la cual es anfitriona Jules. Seth est� interesado en ella mientras que Evan est� enamorado de Becca quien tambi�n siente algo por Evan. Sin embargo Seth tiene un profundo odio hacia ella ya que en cuarto grado ella vio uno de los dibujos de penes que Seth hac�a y lo acus� con el director. Fogell compr� su nueva licencia falsa, que lo llama Mclovin'', con la intenci�n de comprar alcohol para la fiesta.','imagenes/Pelicula2.jpg','https://www.youtube.com/embed/au2Zq8D9RaY','1:26',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (3,'Comando Especial','Schmidt (Jonah Hill) y Jenko (Channing Tatum) son una pareja de reci�n graduados polic�as que trabajan en la labor de polic�as de parque y que al intentar atrapar a unos traficantes de drogas y no decirles sus derechos Miranda tienen que dejarlos ir por lo que su jefe los manda de encubiertos a una escuela para que encuentren a la persona que vende drogas en la escuela. Schmidt y Jenko cambian sus nombres por los de Doug (Schmidt) y Brad (Jenko). Cuando llegan a la escuela hay un cambio en sus horarios y Doug va a las clases de teatro de Brad y Brad a las de qu�mica de Doug. Doug se hace popular y hace amistad con Erick (Dave Franco) uno de los distribuidores de drogas y con su novia Molly (Brie Larson), mientras Brad se hace amigo de los nerds de qu�mica. El par de amigos hacen una fiesta en su casa a la que acuden todos. Brad coge el celular de Erick y lo configura para que pueda escuchar todo lo que Erick hable. Para su mala suerte llegan los padres de Schmidt y hacen que todos salgan corriendo de la fiesta. Doug se gana la confianza de Erick y este le pide que lo ayude a distribuir la droga.','imagenes/Pelicula3.jpg','https://www.youtube.com/embed/XzkEwkftQ8o','1:40',3);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (4,'El Justiciero','Denzel Washington interpreta a McCall, un ex comando de operaciones especiales que fingi� su muerte para tener una vida tranquila en Boston. Cuando sale de su retiro autoimpuesto para rescatar a una joven llamada Teri (Chlo� Grace Moretz), �l se enfrenta cara a cara con mafiosos rusos ultra violentos.','imagenes/Pelicula4.jpg','https://www.youtube.com/embed/ih8laWjsPMk','1:42',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (5,'Easy A',' La estudiante de secundaria Olive Penderghast (Emma Stone) miente a su amiga Rhiannon (Alyson Michalka) dici�ndole que tuvo una cita con un estudiante universitario y que perdi� su virginidad con �l. Sin embargo, la conversaci�n es escuchada por la presidenta del grupo cristiano de la escuela, Marianne (Amanda Bynes). El rumor se extiende r�pidamente por toda la escuela, por lo que Olive decide aprovechar su nueva reputaci�n para ayudar a su amigo homosexual Brandon (Dan Byrd). Olive pretende tener relaciones con �l durante una fiesta para ganar el respeto de los dem�s estudiantes y as� no ser abusado por sus compa�eros homofobos, logrando su cometido.  ','imagenes/Pelicula5.jpg ','https://www.youtube.com/embed/FZGvAe8vgjQ','1:25',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (6,'Taken 3',' Despu�s de una breve visita a su hija, Kim, Bryan Mills se encuentra con su ex esposa, Leonore. Ella le dice que ella y su marido, Stuart St. John, est�n teniendo problemas maritales, dici�ndole que quiere dejar todo y fantasea, �l le entrega una copia de la llave de su dpto. para cuando ella necesite despejar su mente y estar sola. Ella ha reavivado sentimientos rom�nticos por Bryan que se niega a involucrarse mientras ella todav�a est� casada. M�s tarde, Stuart advierte a Bryan que se mantenga alejado de Lenore, por que seg�n �l su acercamiento perjudica al matrimonio.','imagenes/Pelicula6.jpg','https://www.youtube.com/embed/5AGqtiQMOis','1:34',3);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (7,'Los Calienta Bancas','Gus, Richie y Clark son tres muchachos que de ni�os pasaban mucho tiempo en el banquillo y que ahora se les presenta una segunda oportunidad. Mel es un multimillonario de la zona cuyo hijo Nelson constantemente es intimidado por otros chicos. Un d�a, Gus y Clark rescatan a Nelson de las garras de un grupo de matones en el campo de b�isbol. Cuando Max le explica a su padre el incidente, Mel hace a Gus, Clark y Richie una propuesta tentadora: aportar� el dinero necesario para que viajen y jueguen en un torneo contra unos j�venes jugadores de b�isbol para ayudar a su hijo y a otros pazguatos como �l a vengarse de los prepotentes de este mundo. Se llamar� Torneo de Mel de Peque�os Jugadores de B�isbol y Tres Viejos Amigos. Al equipo ganador se le construir� el estadio m�s grande del mundo y, por supuesto, Mel correr� con los gastos. Con el fin de preparar a Gus, Richie y Clark para el torneo, Mel contrata a la leyenda del b�isbol Reggie Jackson para que los entrene','imagenes/Pelicula7.jpg','https://www.youtube.com/embed/TXVboydkkOI','1:20',4);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (8,'Vacaciones En Familia','Jim (Adam Sandler) y Lauren (Drew Barrymore) son dos padres solteros que se conocen en una cita a ciegas en Hooters, la cual resulta ser un completo desastre y por ende, Jim y Lauren acuerdan en no volver a verse m�s. Sin embargo, Jim y Lauren planean ir de vacaciones a �frica por separado con sus familias. Entonces los dos clanes se encuentran en Sud�frica, donde tendr�n que compartir habitaciones en un lujoso hotel rom�ntico, y las hijas de Jim: Hillary, una adolescente confundida muchas veces con un hombre, Espn, una chica que se la pasa hablando con su madre imaginaria (ya que hab�a muerto debido al c�ncer) y Lou, la m�s peque�a que m�s adora a su padre y los de Lauren: Brendan, un ni�o pervertido enamorado de su ni�era y Tyler, un ni�o travieso y amante del beisbol, aunque al principio no se lleven muy bien, pronto se ir�n relacionando mejor hasta que todos terminen como una sola familia.','imagenes/Pelicula8.jpg','https://www.youtube.com/embed/CmJSWMNG5ec','1:38',1);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (9,'Ted','La pelicula empieza en 1985, donde John Bennett, un ni�o solitario residente en un suburbio al sur de Boston pide por Navidad que su oso de peluche, Teddy (Ted) (Seth MacFarlane) cobre vida para ser su mejor amigo.

Veintisiete a�os despues, John (Mark Wahlberg) y Ted contin�an viviendo en Boston de manera despreocupada, incluso a pesar de buscar una relacion estable con Lori Collins (Mila Kunis), sin embargo John debe apartar a Ted de su vida. ','imagenes/Pelicula9.jpg','https://www.youtube.com/embed/xECjE4gR5Ow','1:50',5);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (10,'El Hobbit 3','Bilbo y los enanos se refugian en la Colina del Cuervo mientras el drag�n Smaug desata el infierno sobre la Ciudad del Lago. Bardo, que intento acabar con el drag�n infructuosamente empleando flechas que no atravesaban su piel, con la ayuda de su hijo Bain, logr� matar al drag�n con la �ltima Flecha Negra que quedaba tras comprobar que su antepasado Girion hab�a conseguido hacer lo que todos cre�an que era una leyenda (conseguir arrancar una escama al drag�n, lo que permiti� a Bardo matarlo), y el cuerpo del drag�n cay� sobre la balsa del Gobernador, quien estaba huyendo de la ciudad con todo el oro. Los sobrevivientes escogen a Bardo como su nuevo l�der y buscan refugio en las ruinas de la ciudad de Valle. Kili confiesa su amor por Tauriel antes de partir con Fili, Oin y Bofur hacia la monta�a para reencontrarse con la compa��a, donde descubren que Thorin est� afectado por el "mal del drag�n" mientras busca desesperado la Piedra del Arca. Tras recordar palabras del drag�n y recibir consejo de Balin, Bilbo, quien posee en secreto la Piedra del Arca, decide ocultarla de un demente Thorin, quien llega a desconfiar de sus propios compa�eros.','imagenes/Pelicula10.jpg','https://www.youtube.com/embed/MrHMOcr7YVU','2:00',1);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (11,'Divergente','La historia sigue a Beatrice Prior, una chica de 16 a�os, que vive en una sociedad que ha decidido agrupar a las personas en cinco facciones que tratan de erradicar los males que les llevaron a la guerra: quienes culpaban a la agresividad, crearon Cordialidad; los que culpaban a la ignorancia, se agruparon en Erudici�n; Verdad surgi� de aquellos que estaban en contra del enga�o; contra el ego�smo se fund� Abnegaci�n, y contra la cobard�a, Osad�a. A los diecis�is a�os, los chicos deben hacer una prueba que determinar� a cu�l de las cinco facciones pertenecen; Beatrice decide abandonar su facci�n (Abnegaci�n) pues no sabe si es lo suficientemente altruista como para dedicar su vida a los dem�s. La dif�cil elecci�n de Beatrice marca el inicio de la historia, ya que ahora ella debe hacer frente a las pruebas de iniciaci�n de la facci�n que ha elegido (Osad�a), donde har� aliados y poderosos enemigos, a�n as�, Beatrice debe cuidar que nadie conozca que ella en realidad es una Divergente, es decir, pertenece a m�s de dos facciones y representa un peligro para la sociedad.','imagenes/Pelicula11.jpg','https://www.youtube.com/embed/e8UHUAdBl84','1:55',1);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (12,'Bob Esponja: Un Heroe Fuera Del Mar','En esta ocasi�n Bob Esponja y c�a se embarcan en una aventura en la que deber�n encontrar una receta robada, lo que llevar� a los personajes de Fondo Bikini hasta nuestra dimensi�n. Por lo que la pel�cula mezcla animaci�n y escenas de acci�n real. Las escenas de animaci�n est�n hechas tanto con CGI (imagen generada por ordenador) como en dibujo a mano. ','imagenes/Pelicula12.jpg','https://www.youtube.com/embed/dsPaMsEuetk','1:34',1);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (13,'La Milla Verde','La pel�cula narra la vida de Paul Edgecomb, que siendo un anciano de 108 a�os, cuenta su historia como oficial del �corredor de la muerte� en la penitenciar�a Cold Mountain, en el estado de Luisiana, durante la d�cada de 1930. Edgecomb tiene entre sus presos a un personaje con poderes sobrenaturales que es capaz de sanar a personas y hacer otros tipos de actos paranormales. ','imagenes/Pelicula13.jpg','https://www.youtube.com/embed/0zkms0Tkmus','1:59',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (14,'El ni�o de la Pijama de Rayas','Bruno, un ni�o alem�n de ocho a�os durante el r�gimen nazi en la Segunda Guerra Mundial, es hijo de un estricto comandante que acaba de ser asignado a su nuevo puesto en un campo de concentraci�n. Bruno, que convive junto con su hermana mayor Gretel en un barrio acomodado de Berl�n, se ve entonces repentinamente obligado a mudarse, traslad�ndose a un lugar aislado llamado "Auschwitz". Bruno se desespera por no encontrar ning�n amigo con quien jugar en su nueva casa, m�s peque�a y con menos terreno para explorar. Desde su ventana se puede ver un grupo de casas tras una valla y mucha gente vestida con pijamas de rayas. Todos los que all� se encuentran son jud�os confinados en el campo de concentraci�n de Auschwitz. Bruno, en su inocencia, da por supuesto que son granjeros. ','imagenes/Pelicula14.jpg','https://www.youtube.com/embed/rzow19gyNqQ','2:05',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (15,'Una Esposa de Mentiras','Danny (Adam Sandler) es un exitoso cirujano pl�stico, despreciado en su juventud por su deforme nariz, que finge ser un infeliz marido para ligar con mujeres y as� superar su trauma. Cuando finalmente conoce a una chica llamada Palmer (Brooklyn Decker) de la que se enamora, y tras acostarse juntos ella descubre la alianza de boda que �l utiliza. Por miedo de decirle la verdad (y como ha hecho desde hacia varios a�os), �l le dice que est� en proceso de divorcio, a lo que ella le responde que quiere conocer a su futura ex-esposa. As� que Danny le pide a su ayudante, Katherine (Jennifer Aniston), que se haga pasar por su esposa con el fin de probar su historia, pero una mentira se convierte en otra, lo que lleva a los ni�os de Katherine al embrollo. Finalmente en un viaje a Haw�i todos cambiar�n sus vidas.','imegenes/Pelicula15.jpg','https://www.youtube.com/embed/xuZnmYjAKww','1:45',1);


SELECT * FROM Pelicula;
/*
Desde aqui vamos a crear

*/
CREATE TABLE Categoria(
  idCategoria INT not null,
  nombreCategoria VARCHAR(10) not null,
  descriptionCategoria VARCHAR(255) not null,
  PRIMARY KEY(idCategoria)
) 

CREATE SEQUENCE categ_seq;

CREATE OR REPLACE TRIGGER Categoria_bir 
BEFORE INSERT ON Categoria
FOR EACH ROW
CREATE TABLE Complejos(
  ID           NUMBER(10)    NOT NULL,
  nombre       VARCHAR(50)  NOT NULL,
  direccion    VARCHAR(200)  NOT NULL,
  lat      VARCHAR(10)   NOT NULL,
  lon     VARCHAR(10)   NOT NULL,
  CONSTRAINT complejos_pk PRIMARY KEY (ID)
);

CREATE SEQUENCE complejos_seq;

CREATE OR REPLACE TRIGGER complejos_bir 
BEFORE INSERT ON Complejos 
FOR EACH ROW

BEGIN
  SELECT complejos_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO Complejos VALUES(null, 'Oakland','zona 10 , Guatemala, Guatemala','50.54545', '14.5456465' );
INSERT INTO Complejos VALUES(null, 'Miraflores','zona 7, Guatemala, Guatemala','50.54545', '14.5456465' );
INSERT INTO Complejos VALUES(null, 'Cayala','zona 16, Guatemala, Guatemala','50.54545', '14.5456465' );
INSERT INTO Complejos VALUES(null, 'Portales','zona 18, Guatemala, Guatemala','50.54545', '14.5456465' );

SELECT * FROM Complejos;


CREATE TABLE Sala(
  ID           NUMBER(10)    NOT NULL,
  nombre       VARCHAR(50)   NOT NULL,
  tipo         VARCHAR(200)  NOT NULL,
  idComplejo   NUMBER(10)    NOT NULL,
  CONSTRAINT sala_pk PRIMARY KEY (ID),  
  CONSTRAINT Sala_fk FOREIGN KEY (idComplejo) REFERENCES Complejos(ID) ON DELETE CASCADE
);

CREATE SEQUENCE sala_seq;

CREATE OR REPLACE TRIGGER sala_bir 
BEFORE INSERT ON Sala 
FOR EACH ROW

BEGIN
  SELECT Sala_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/* Complejo 1*/
INSERT INTO Sala VALUES(null, '1','3D', 1 );
INSERT INTO Sala VALUES(null, '2','3D', 1 );
INSERT INTO Sala VALUES(null, '3','3D', 1 );
INSERT INTO Sala VALUES(null, '1n','Normal', 1 );
INSERT INTO Sala VALUES(null, '2n','Normal', 1 );
INSERT INTO Sala VALUES(null, '3n','Normal', 1 );
INSERT INTO Sala VALUES(null, '4n','Normal', 1 );
INSERT INTO Sala VALUES(null, '5n','Normal', 1 );
INSERT INTO Sala VALUES(null, '6n','Normal', 1 );


/* complejo 2*/
INSERT INTO Sala VALUES(null, '1','3D', 2 );
INSERT INTO Sala VALUES(null, '2','3D', 2 );
INSERT INTO Sala VALUES(null, '3n','Normal', 2 );
INSERT INTO Sala VALUES(null, '4n','Normal', 2 );
INSERT INTO Sala VALUES(null, '5n','Normal', 2 );
INSERT INTO Sala VALUES(null, '6n','Normal', 2 );

/*Complejo 3 */
INSERT INTO Sala VALUES(null, '1n','Normal', 3 );
INSERT INTO Sala VALUES(null, '2n','Normal', 3 );
INSERT INTO Sala VALUES(null, '3n','Normal', 3 );
INSERT INTO Sala VALUES(null, '4n','Normal', 3 );
INSERT INTO Sala VALUES(null, '5n','Normal', 3 );
INSERT INTO Sala VALUES(null, '6n','Normal', 3 );
INSERT INTO Sala VALUES(null, '7n','Normal', 3 );
INSERT INTO Sala VALUES(null, '8n','Normal', 3 );

/*complejo 4*/
INSERT INTO Sala VALUES(null, '1','3D', 4 );
INSERT INTO Sala VALUES(null, '2','3D', 4 );
INSERT INTO Sala VALUES(null, '3','3D', 4 );
INSERT INTO Sala VALUES(null, '4','3D', 4 );
INSERT INTO Sala VALUES(null, '5','3D', 4 );
INSERT INTO Sala VALUES(null, '6','3D', 4 );
INSERT INTO Sala VALUES(null, '1n','Normal', 4 );
INSERT INTO Sala VALUES(null, '2n','Normal', 4 );
INSERT INTO Sala VALUES(null, '3n','Normal', 4 );
INSERT INTO Sala VALUES(null, '4n','Normal', 4 );
SELECT * FROM Sala;


CREATE TABLE Clasificacion(
  ID           NUMBER(10)    NOT NULL,
  nombre       VARCHAR(50)   NOT NULL,
  descripcion  VARCHAR2(500)  NOT NULL,
  CONSTRAINT clasificacion_pk PRIMARY KEY (ID)
);

CREATE SEQUENCE clasificacion_seq;

CREATE OR REPLACE TRIGGER cladificacion_bir 
BEFORE INSERT ON Clasificacion
FOR EACH ROW

BEGIN
  SELECT Clasificacion_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

INSERT INTO Clasificacion VALUES(null, 'TP','Todo Publico');
INSERT INTO Clasificacion VALUES(null, 'A','Menores de 12 acompa�ados por adulto');
INSERT INTO Clasificacion VALUES(null, 'B','Mayores de 12 A�os');
INSERT INTO Clasificacion VALUES(null, 'C','Mayores de 15 A�os');
INSERT INTO Clasificacion VALUES(null, 'D','Mayores de 18 A�os');

SELECT * FROM CLASIFICACION;


CREATE TABLE Pelicula(
  ID              NUMBER(10)    NOT NULL,
  nombre          VARCHAR2(255)   NOT NULL,
  descripcion     VARCHAR2(1200)  NOT NULL,
  imagen          VARCHAR2(255)   NOT NULL,
  trailer         VARCHAR2(500)  NOT NULL,
  duracion        VARCHAR2(255)  NOT NULL,
  idClasificacion NUMBER(10)    NOT NULL,
  CONSTRAINT Pelicula_pk PRIMARY KEY (ID),
  CONSTRAINT Pelicula_fk FOREIGN KEY (idClasificacion) REFERENCES Clasificacion(ID) ON DELETE CASCADE
);

CREATE SEQUENCE pelicula_seq;

CREATE OR REPLACE TRIGGER pelicula_bir 
BEFORE INSERT ON Pelicula
FOR EACH ROW

BEGIN
  SELECT pelicula_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;


Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (1,'Rompiendo Las Reglas','Jake Tyler recientemente se ha mudado a Orlando, Florida, con su familia para apoyar a su hermano menor en una carrera como tenista profesional. Jake era una estrella atleta en el equipo de f�tbol de su casa, pero en esta nueva ciudad es un extranjero con una reputaci�n por ser un luchador de mal genio despu�s de ver un v�deo de �l en Internet comenzando una pelea en un juego de f�tbol, porque uno de sus oponentes hizo un comentario sobre su padre muerto, y �ste v�deo circula en su nueva escuela. En dicho video sale Bole, conocido ladr�n de los alrededores.
Tratando de hacer un intento por encajar, en la invitaci�n de una compa�era de clase, Baja Miller, Jake va a una fiesta donde es obligado a una pelea con el campe�n de artes marciales ','imagenes/Pelicula1.jpg','https://www.youtube.com/embed/WTVTcDe5TXM','1:35',3);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (2,'Super Cool',' Los chicos Seth, Evan y Fogell (m�s adelante llamado "McLovin") tienen como destino una fiesta de la cual es anfitriona Jules. Seth est� interesado en ella mientras que Evan est� enamorado de Becca quien tambi�n siente algo por Evan. Sin embargo Seth tiene un profundo odio hacia ella ya que en cuarto grado ella vio uno de los dibujos de penes que Seth hac�a y lo acus� con el director. Fogell compr� su nueva licencia falsa, que lo llama Mclovin'', con la intenci�n de comprar alcohol para la fiesta.','imagenes/Pelicula2.jpg','https://www.youtube.com/embed/au2Zq8D9RaY','1:26',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (3,'Comando Especial','Schmidt (Jonah Hill) y Jenko (Channing Tatum) son una pareja de reci�n graduados polic�as que trabajan en la labor de polic�as de parque y que al intentar atrapar a unos traficantes de drogas y no decirles sus derechos Miranda tienen que dejarlos ir por lo que su jefe los manda de encubiertos a una escuela para que encuentren a la persona que vende drogas en la escuela. Schmidt y Jenko cambian sus nombres por los de Doug (Schmidt) y Brad (Jenko). Cuando llegan a la escuela hay un cambio en sus horarios y Doug va a las clases de teatro de Brad y Brad a las de qu�mica de Doug. Doug se hace popular y hace amistad con Erick (Dave Franco) uno de los distribuidores de drogas y con su novia Molly (Brie Larson), mientras Brad se hace amigo de los nerds de qu�mica. El par de amigos hacen una fiesta en su casa a la que acuden todos. Brad coge el celular de Erick y lo configura para que pueda escuchar todo lo que Erick hable. Para su mala suerte llegan los padres de Schmidt y hacen que todos salgan corriendo de la fiesta. Doug se gana la confianza de Erick y este le pide que lo ayude a distribuir la droga.','imagenes/Pelicula3.jpg','https://www.youtube.com/embed/XzkEwkftQ8o','1:40',3);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (4,'El Justiciero','Denzel Washington interpreta a McCall, un ex comando de operaciones especiales que fingi� su muerte para tener una vida tranquila en Boston. Cuando sale de su retiro autoimpuesto para rescatar a una joven llamada Teri (Chlo� Grace Moretz), �l se enfrenta cara a cara con mafiosos rusos ultra violentos.','imagenes/Pelicula4.jpg','https://www.youtube.com/embed/ih8laWjsPMk','1:42',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (5,'Easy A',' La estudiante de secundaria Olive Penderghast (Emma Stone) miente a su amiga Rhiannon (Alyson Michalka) dici�ndole que tuvo una cita con un estudiante universitario y que perdi� su virginidad con �l. Sin embargo, la conversaci�n es escuchada por la presidenta del grupo cristiano de la escuela, Marianne (Amanda Bynes). El rumor se extiende r�pidamente por toda la escuela, por lo que Olive decide aprovechar su nueva reputaci�n para ayudar a su amigo homosexual Brandon (Dan Byrd). Olive pretende tener relaciones con �l durante una fiesta para ganar el respeto de los dem�s estudiantes y as� no ser abusado por sus compa�eros homofobos, logrando su cometido.  ','imagenes/Pelicula5.jpg ','https://www.youtube.com/embed/FZGvAe8vgjQ','1:25',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (6,'Taken 3',' Despu�s de una breve visita a su hija, Kim, Bryan Mills se encuentra con su ex esposa, Leonore. Ella le dice que ella y su marido, Stuart St. John, est�n teniendo problemas maritales, dici�ndole que quiere dejar todo y fantasea, �l le entrega una copia de la llave de su dpto. para cuando ella necesite despejar su mente y estar sola. Ella ha reavivado sentimientos rom�nticos por Bryan que se niega a involucrarse mientras ella todav�a est� casada. M�s tarde, Stuart advierte a Bryan que se mantenga alejado de Lenore, por que seg�n �l su acercamiento perjudica al matrimonio.','imagenes/Pelicula6.jpg','https://www.youtube.com/embed/5AGqtiQMOis','1:34',3);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (7,'Los Calienta Bancas','Gus, Richie y Clark son tres muchachos que de ni�os pasaban mucho tiempo en el banquillo y que ahora se les presenta una segunda oportunidad. Mel es un multimillonario de la zona cuyo hijo Nelson constantemente es intimidado por otros chicos. Un d�a, Gus y Clark rescatan a Nelson de las garras de un grupo de matones en el campo de b�isbol. Cuando Max le explica a su padre el incidente, Mel hace a Gus, Clark y Richie una propuesta tentadora: aportar� el dinero necesario para que viajen y jueguen en un torneo contra unos j�venes jugadores de b�isbol para ayudar a su hijo y a otros pazguatos como �l a vengarse de los prepotentes de este mundo. Se llamar� Torneo de Mel de Peque�os Jugadores de B�isbol y Tres Viejos Amigos. Al equipo ganador se le construir� el estadio m�s grande del mundo y, por supuesto, Mel correr� con los gastos. Con el fin de preparar a Gus, Richie y Clark para el torneo, Mel contrata a la leyenda del b�isbol Reggie Jackson para que los entrene','imagenes/Pelicula7.jpg','https://www.youtube.com/embed/TXVboydkkOI','1:20',4);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (8,'Vacaciones En Familia','Jim (Adam Sandler) y Lauren (Drew Barrymore) son dos padres solteros que se conocen en una cita a ciegas en Hooters, la cual resulta ser un completo desastre y por ende, Jim y Lauren acuerdan en no volver a verse m�s. Sin embargo, Jim y Lauren planean ir de vacaciones a �frica por separado con sus familias. Entonces los dos clanes se encuentran en Sud�frica, donde tendr�n que compartir habitaciones en un lujoso hotel rom�ntico, y las hijas de Jim: Hillary, una adolescente confundida muchas veces con un hombre, Espn, una chica que se la pasa hablando con su madre imaginaria (ya que hab�a muerto debido al c�ncer) y Lou, la m�s peque�a que m�s adora a su padre y los de Lauren: Brendan, un ni�o pervertido enamorado de su ni�era y Tyler, un ni�o travieso y amante del beisbol, aunque al principio no se lleven muy bien, pronto se ir�n relacionando mejor hasta que todos terminen como una sola familia.','imagenes/Pelicula8.jpg','https://www.youtube.com/embed/CmJSWMNG5ec','1:38',1);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (9,'Ted','La pelicula empieza en 1985, donde John Bennett, un ni�o solitario residente en un suburbio al sur de Boston pide por Navidad que su oso de peluche, Teddy (Ted) (Seth MacFarlane) cobre vida para ser su mejor amigo.

Veintisiete a�os despues, John (Mark Wahlberg) y Ted contin�an viviendo en Boston de manera despreocupada, incluso a pesar de buscar una relacion estable con Lori Collins (Mila Kunis), sin embargo John debe apartar a Ted de su vida. ','imagenes/Pelicula9.jpg','https://www.youtube.com/embed/xECjE4gR5Ow','1:50',5);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (10,'El Hobbit 3','Bilbo y los enanos se refugian en la Colina del Cuervo mientras el drag�n Smaug desata el infierno sobre la Ciudad del Lago. Bardo, que intento acabar con el drag�n infructuosamente empleando flechas que no atravesaban su piel, con la ayuda de su hijo Bain, logr� matar al drag�n con la �ltima Flecha Negra que quedaba tras comprobar que su antepasado Girion hab�a conseguido hacer lo que todos cre�an que era una leyenda (conseguir arrancar una escama al drag�n, lo que permiti� a Bardo matarlo), y el cuerpo del drag�n cay� sobre la balsa del Gobernador, quien estaba huyendo de la ciudad con todo el oro. Los sobrevivientes escogen a Bardo como su nuevo l�der y buscan refugio en las ruinas de la ciudad de Valle. Kili confiesa su amor por Tauriel antes de partir con Fili, Oin y Bofur hacia la monta�a para reencontrarse con la compa��a, donde descubren que Thorin est� afectado por el "mal del drag�n" mientras busca desesperado la Piedra del Arca. Tras recordar palabras del drag�n y recibir consejo de Balin, Bilbo, quien posee en secreto la Piedra del Arca, decide ocultarla de un demente Thorin, quien llega a desconfiar de sus propios compa�eros.','imagenes/Pelicula10.jpg','https://www.youtube.com/embed/MrHMOcr7YVU','2:00',1);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (11,'Divergente','La historia sigue a Beatrice Prior, una chica de 16 a�os, que vive en una sociedad que ha decidido agrupar a las personas en cinco facciones que tratan de erradicar los males que les llevaron a la guerra: quienes culpaban a la agresividad, crearon Cordialidad; los que culpaban a la ignorancia, se agruparon en Erudici�n; Verdad surgi� de aquellos que estaban en contra del enga�o; contra el ego�smo se fund� Abnegaci�n, y contra la cobard�a, Osad�a. A los diecis�is a�os, los chicos deben hacer una prueba que determinar� a cu�l de las cinco facciones pertenecen; Beatrice decide abandonar su facci�n (Abnegaci�n) pues no sabe si es lo suficientemente altruista como para dedicar su vida a los dem�s. La dif�cil elecci�n de Beatrice marca el inicio de la historia, ya que ahora ella debe hacer frente a las pruebas de iniciaci�n de la facci�n que ha elegido (Osad�a), donde har� aliados y poderosos enemigos, a�n as�, Beatrice debe cuidar que nadie conozca que ella en realidad es una Divergente, es decir, pertenece a m�s de dos facciones y representa un peligro para la sociedad.','imagenes/Pelicula11.jpg','https://www.youtube.com/embed/e8UHUAdBl84','1:55',1);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (12,'Bob Esponja: Un Heroe Fuera Del Mar','En esta ocasi�n Bob Esponja y c�a se embarcan en una aventura en la que deber�n encontrar una receta robada, lo que llevar� a los personajes de Fondo Bikini hasta nuestra dimensi�n. Por lo que la pel�cula mezcla animaci�n y escenas de acci�n real. Las escenas de animaci�n est�n hechas tanto con CGI (imagen generada por ordenador) como en dibujo a mano. ','imagenes/Pelicula12.jpg','https://www.youtube.com/embed/dsPaMsEuetk','1:34',1);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (13,'La Milla Verde','La pel�cula narra la vida de Paul Edgecomb, que siendo un anciano de 108 a�os, cuenta su historia como oficial del �corredor de la muerte� en la penitenciar�a Cold Mountain, en el estado de Luisiana, durante la d�cada de 1930. Edgecomb tiene entre sus presos a un personaje con poderes sobrenaturales que es capaz de sanar a personas y hacer otros tipos de actos paranormales. ','imagenes/Pelicula13.jpg','https://www.youtube.com/embed/0zkms0Tkmus','1:59',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (14,'El ni�o de la Pijama de Rayas','Bruno, un ni�o alem�n de ocho a�os durante el r�gimen nazi en la Segunda Guerra Mundial, es hijo de un estricto comandante que acaba de ser asignado a su nuevo puesto en un campo de concentraci�n. Bruno, que convive junto con su hermana mayor Gretel en un barrio acomodado de Berl�n, se ve entonces repentinamente obligado a mudarse, traslad�ndose a un lugar aislado llamado "Auschwitz". Bruno se desespera por no encontrar ning�n amigo con quien jugar en su nueva casa, m�s peque�a y con menos terreno para explorar. Desde su ventana se puede ver un grupo de casas tras una valla y mucha gente vestida con pijamas de rayas. Todos los que all� se encuentran son jud�os confinados en el campo de concentraci�n de Auschwitz. Bruno, en su inocencia, da por supuesto que son granjeros. ','imagenes/Pelicula14.jpg','https://www.youtube.com/embed/rzow19gyNqQ','2:05',2);
Insert into PELICULA (ID,NOMBRE,DESCRIPCION,IMAGEN,TRAILER,DURACION,IDCLASIFICACION) values (15,'Una Esposa de Mentiras','Danny (Adam Sandler) es un exitoso cirujano pl�stico, despreciado en su juventud por su deforme nariz, que finge ser un infeliz marido para ligar con mujeres y as� superar su trauma. Cuando finalmente conoce a una chica llamada Palmer (Brooklyn Decker) de la que se enamora, y tras acostarse juntos ella descubre la alianza de boda que �l utiliza. Por miedo de decirle la verdad (y como ha hecho desde hacia varios a�os), �l le dice que est� en proceso de divorcio, a lo que ella le responde que quiere conocer a su futura ex-esposa. As� que Danny le pide a su ayudante, Katherine (Jennifer Aniston), que se haga pasar por su esposa con el fin de probar su historia, pero una mentira se convierte en otra, lo que lleva a los ni�os de Katherine al embrollo. Finalmente en un viaje a Haw�i todos cambiar�n sus vidas.','imegenes/Pelicula15.jpg','https://www.youtube.com/embed/xuZnmYjAKww','1:45',1);


SELECT * FROM Pelicula;

CREATE TABLE Categoria(
  idCategoria INT not null,
  nombreCategoria VARCHAR(10) not null,
  descriptionCategoria VARCHAR(255) not null,
  PRIMARY KEY(idCategoria)
) 

CREATE SEQUENCE categ_seq;

CREATE OR REPLACE TRIGGER Categoria_bir 
BEFORE INSERT ON Categoria
FOR EACH ROW

BEGIN
  SELECT categ_seq.NEXTVAL
  INTO   :new.idCategoria
  FROM   dual;
END;


CREATE TABLE Funcion(
  idFuncion INT not null,
  nombreFuncion VARCHAR(25) not null,
  descripcionFuncion VARCHAR(255) not null,
  presioFuncion VARCHAR(255) not null,
  PRIMARY KEY(idFuncion)
)

CREATE SEQUENCE funcion_seq;

CREATE OR REPLACE TRIGGER Funcion_bir 
BEFORE INSERT ON Funcion
FOR EACH ROW

BEGIN
  SELECT funcion_seq.NEXTVAL
  INTO   :new.idFuncion
  FROM   dual;
END;

CREATE TABLE Venta(
  idVenta INT not null,
  nombrePersona VARCHAR(25) not null,
  numeroTransaccion INT not null,
  correoPersona VARCHAR(25) not null,
  telefonoPersona int not null,
  tipoPago VARCHAR(25) not null,
  PRIMARY KEY(idVenta)
)

CREATE SEQUENCE venta_seq;

CREATE OR REPLACE TRIGGER Venta_bir 
BEFORE INSERT ON Venta
FOR EACH ROW

BEGIN
  SELECT venta_seq.NEXTVAL
  INTO   :new.idVenta
  FROM   dual;
END;


CREATE TABLE Asiento(
  idAsiento INT not null,
  idSala INT not null,
  nombreAsiento VARCHAR(10) not null,
  PRIMARY KEY(idAsiento),
  CONSTRAINT Asiento_fk FOREIGN KEY(idSala) REFERENCES Sala(id)
)

CREATE SEQUENCE Asiento_seq;

CREATE OR REPLACE TRIGGER Asiento_bir 
BEFORE INSERT ON Asiento
FOR EACH ROW

BEGIN
  SELECT Asiento_seq.NEXTVAL
  INTO   :new.idAsiento
  FROM   dual;
END;


CREATE TABLE Cartelera(
  idCartelera INT not null,
  idSala INT not null,
  idPelicula INT not null,
  horaInicio NUMBER(6,2) not null,
  horaFin NUMBER(6,2) not null,
  PRIMARY KEY(idCartelera),
  CONSTRAINT PeliculaSala_fk FOREIGN KEY(idSala) REFERENCES Sala(id),
  CONSTRAINT CarteleraPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(id)
)

CREATE SEQUENCE cartelera_seq;

CREATE OR REPLACE TRIGGER Cartelera_bir 
BEFORE INSERT ON Cartelera
FOR EACH ROW

BEGIN
  SELECT cartelera_seq.NEXTVAL
  INTO   :new.idCartelera
  FROM   dual;
END;

CREATE TABLE PeliculaCategoria(
  idPelicula INT not null,
  idCategoria INT NOT NULL,
  CONSTRAINT CategoriaPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(id),
  CONSTRAINT PeliculaCateria_fk FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
)

CREATE TABLE VentaDetalle(
  idVenta INT not null,
  idAsiento INT not null,
  idCartelera INT not null,
  idFuncion INT not null,
  CONSTRAINT VentaDetalleVenta_fk FOREIGN KEY(idVenta) REFERENCES Venta(idVenta),
  CONSTRAINT VentaDetalleAsiento_fk FOREIGN KEY(idAsiento) REFERENCES Asiento(idAsiento),
  CONSTRAINT VentaCartelera_fk FOREIGN KEY(idCartelera) REFERENCES Cartelera(idCartelera),
  CONSTRAINT VentaFuncion_fk FOREIGN KEY(idFuncion) REFERENCES Funcion(idFuncion)
)

CREATE TABLE EstrenoPelicula(
  idEstreno INT not null,
  idPelicula INT not null,
  fechaPelicula DATE not null,
  PRIMARY KEY(idEstreno),
  CONSTRAINT EstrenoPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(id)
)

CREATE SEQUENCE Estreno_seq;

CREATE OR REPLACE TRIGGER Estreno_bir 
BEFORE INSERT ON EstrenoPelicula
FOR EACH ROW

BEGIN
  SELECT Estreno_seq.NEXTVAL
  INTO   :new.idEstreno
  FROM   dual;
END;

CREATE TABLE Funcion(
  idFuncion INT not null,
  nombreFuncion VARCHAR(25) not null,
  descripcionFuncion VARCHAR(255) not null,
  presioFuncion VARCHAR(255) not null,
  PRIMARY KEY(idFuncion)
)

CREATE SEQUENCE funcion_seq;

CREATE OR REPLACE TRIGGER Funcion_bir 
BEFORE INSERT ON Funcion
FOR EACH ROW

BEGIN
  SELECT funcion_seq.NEXTVAL
  INTO   :new.idFuncion
  FROM   dual;
END;

CREATE TABLE Venta(
  idVenta INT not null,
  nombrePersona VARCHAR(25) not null,
  numeroTransaccion INT not null,
  correoPersona VARCHAR(25) not null,
  telefonoPersona int not null,
  tipoPago VARCHAR(25) not null,
  PRIMARY KEY(idVenta)
)

CREATE SEQUENCE venta_seq;

CREATE OR REPLACE TRIGGER Venta_bir 
BEFORE INSERT ON Venta
FOR EACH ROW

BEGIN
  SELECT venta_seq.NEXTVAL
  INTO   :new.idVenta
  FROM   dual;
END;


CREATE TABLE Asiento(
  idAsiento INT not null,
  idSala INT not null,
  nombreAsiento VARCHAR(10) not null,
  PRIMARY KEY(idAsiento),
  CONSTRAINT Asiento_fk FOREIGN KEY(idSala) REFERENCES Sala(idSala)
)

CREATE SEQUENCE Asiento_seq;

CREATE OR REPLACE TRIGGER Asiento_bir 
BEFORE INSERT ON Asiento
FOR EACH ROW

BEGIN
  SELECT Asiento_seq.NEXTVAL
  INTO   :new.idAsiento
  FROM   dual;
END;


CREATE TABLE Cartelera(
  idCartelera INT not null,
  idSala INT not null,
  idPelicula INT not null,
  horaInicio NUMBER(6,2) not null,
  horaFin NUMBER(6,2) not null,
  PRIMARY KEY(idCartelera),
  CONSTRAINT PeliculaSala_fk FOREIGN KEY(idSala) REFERENCES Sala(idSala),
  CONSTRAINT CarteleraPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula)
)

CREATE SEQUENCE cartelera_seq;

CREATE OR REPLACE TRIGGER Cartelera_bir 
BEFORE INSERT ON Cartelera
FOR EACH ROW

BEGIN
  SELECT cartelera_seq.NEXTVAL
  INTO   :new.idCartelera
  FROM   dual;
END;

CREATE TABLE PeliculaCategoria(
  idPelicula INT not null,
  idCategoria INT NOT NULL,
  CONSTRAINT CategoriaPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula),
  CONSTRAINT PeliculaCateria_fk FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)
)

CREATE TABLE VentaDetalle(
  idVenta INT not null,
  idAsiento INT not null,
  idCartelera INT not null,
  idFuncion INT not null,
  CONSTRAINT VentaDetalleVenta_fk FOREIGN KEY(idVenta) REFERENCES Venta(idVenta),
  CONSTRAINT VentaDetalleAsiento_fk FOREIGN KEY(idAsiento) REFERENCES Asiento(idAsiento),
  CONSTRAINT VentaCartelera_fk FOREIGN KEY(idCartelera) REFERENCES Cartelera(idCartelera),
  CONSTRAINT VentaFuncion_fk FOREIGN KEY(idFuncion) REFERENCES Funcion(idFuncion)
)

CREATE TABLE EstrenoPelicula(
  idEstreno INT not null,
  idPelicula INT not null,
  fechaPelicula DATE not null,
  PRIMARY KEY(idEstreno),
  CONSTRAINT EstrenoPelicula_fk FOREIGN KEY(idPelicula) REFERENCES Pelicula(idPelicula)
)

CREATE SEQUENCE Estreno_seq;

CREATE OR REPLACE TRIGGER Estreno_bir 
BEFORE INSERT ON EstrenoPelicula
FOR EACH ROW

BEGIN
  SELECT Estreno_seq.NEXTVAL
  INTO   :new.idEstreno
  FROM   dual;
END;
