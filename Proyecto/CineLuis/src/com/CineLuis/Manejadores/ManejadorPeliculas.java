package com.CineLuis.Manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.CineLuis.Conexion.*;
import com.CineLuis.Beans.Pelicula;

public class ManejadorPeliculas {

	private static final ManejadorPeliculas INSTANCE = new ManejadorPeliculas();
	
	public static final ManejadorPeliculas getInstance(){
		return INSTANCE;
	}
	
	public Pelicula getPeliculaById(Integer id){
		Pelicula pl = null;
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula WHERE ID = " + id);
		
		try {
			while(result.next()){
				pl = new Pelicula(result.getInt("ID"), result.getString("NOMBRE"), result.getString("DESCRIPCION"), result.getString("IMAGEN"), result.getString("TRAILER"), result.getString("DURACION"), result.getInt("IDCLASIFICACION"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pl;
	}
	
	public ArrayList<Pelicula> getPeliculaList(){
		
		ArrayList<Pelicula> lista = new ArrayList<Pelicula>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM Pelicula");
		
		try {
			while(result.next()){
				lista.add(new Pelicula(result.getInt("ID"), result.getString("nombre"), result.getString("descripcion"), result.getString("imagen"), result.getString("trailer"), result.getString("duracion"), result.getInt("idClasificacion")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lista;
	}
	
	
}
