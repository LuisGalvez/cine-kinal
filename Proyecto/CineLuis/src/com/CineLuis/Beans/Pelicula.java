package com.CineLuis.Beans;

public class Pelicula {
	private Integer ID;
	private String nombre;
	private String descripcion;
	private String imagen;
	private String trailer;
	private String duracion;
	private Integer idClasificacion;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getTrailer() {
		return trailer;
	}
	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public Integer getIdClasificacion() {
		return idClasificacion;
	}
	public void setIdClasificacion(Integer idClasificacion) {
		this.idClasificacion = idClasificacion;
	}
	public Pelicula(Integer iD, String nombre, String descripcion,
			String imagen, String trailer, String duracion,
			Integer idClasificacion) {
		super();
		ID = iD;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.imagen = imagen;
		this.trailer = trailer;
		this.duracion = duracion;
		this.idClasificacion = idClasificacion;
	}
	public Pelicula() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
