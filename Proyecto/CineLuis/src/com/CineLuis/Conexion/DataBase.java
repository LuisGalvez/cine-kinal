package com.CineLuis.Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DataBase {
private static final DataBase INSTANCE = new DataBase();
	
	private Connection conexion;
	private Statement stmt;
	
	public static final DataBase getInstance(){
		return INSTANCE;
	}
	
	private DataBase(){
		try {
			Class.forName("oracle.jdbc.OracleDriver").newInstance();
			conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "LuisCarlos", "luis");
			stmt = conexion.createStatement();
			
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void ejecutarConsulta(String consulta){
		try {
			stmt.execute(consulta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ResultSet obtenerConsulta(String consulta){
		
		ResultSet result = null;
		
		try {
			result = stmt.executeQuery(consulta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

}