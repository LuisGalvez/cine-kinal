<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Proyecto Luis Galvez</title>
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="style/style.css" />
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/simple.carousel.js"></script>
    
    <script type="text/javascript">
        jQuery(document).ready(function() {
            // example 1
           
            // example 2
            $("ul.example2").simplecarousel({
                width:700,
                height:400,
                auto: 4000,
                fade: 400,
                pagination: true
            });
        });
        
    </script>
<link type="text/css" rel="stylesheet" href="Estilos/style0.css"/>
<link type="text/css" rel="stylesheet" href="Estilos/estilos.css"/>
</head>
<body >
	
<div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index.jsp">Luis<span class="logo_colour">Galvez</span></a></h1>
          <h2>Cine Kinal</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li class="selected"><a href="PeliculasServlet.do">Inicio</a></li>
          <li><a href="PeliculasServlet.do">Complejos</a></li>
          <li><a href="PeliculasServlet.do">Peliculas</a></li>
          <li><a href="PeliculasServlet.do">Contacto</a></li>
        </ul>
      </div>
    </div>	
    </div>
    <div class="Car">
    <ul class="example2">
		<c:forEach items="${listado}" var="ps">
			<li>
				<h3 id="des">${ps.getNombre()}</h3>
				<a href="./Pelis.do?ID=${ps.getID()}"><img src="${ps.getImagen()}" /></a>
				<p id="des" class="peque">${ps.getDescripcion()}</p>
			</li>				
		</c:forEach>
  	</ul>
  	</div>
    <div id="site_content">
    	<div id="content">
    	   	<h1>Bienvenido a Cine KINAL</h1>
    	   	
    	   	<p>Cine Kinal ha sido creado para que todo usuario pueda ingresar rapidamente y facilmente a ver las peliculas disponibles, tomando en cuenta el complejo, el tipo de sala, los precios, poder apartar sus asientos y ver tranquilamente la pelicula de su eleccion.</p>
    	   	<h2>Compatibilidad del Navegador</h2>
    	   	<ul>
    	    	<li>Internet Explorer 8</li>
    	    	<li>FireFox 3</li>
    	    	<li>Google Chrome 13</li>
				<li>y mas...</li>
    	    </ul>
		</div>
		
	</div>
	<div id="footer">
    		<p><a href="PeliculasServlet.do">Inicio</a> | <a href="PeliculasServlet.do">Examples</a> | <a href="PeliculasServlet.do">A Page</a> | <a href="PeliculasServlet.do">Another Page</a> | <a href="PeliculasServlet.do">Contact Us</a></p>
    		<p>Copyright &copy; LUIS CARLOS RODRIGUEZ GALVEZ | <a href="http://ww.kinal.org.gt">KINAL</a> | <a href="https://twitter.com/__LuisGalvez">twitter</a> | <a href="https://www.facebook.com/luis.galvez.7771">facebook</a></p>
    	</div>
  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>